DOCKER_OPTS= -v ~/.aws:/home/jenkins/.aws -v `pwd`:/workspace -w /workspace -u 1000:1000 -it vss-policy-build

clean:
	docker rmi -f vss-policy-build
	rm ./bundles/opa-policies.tar.gz
	rm ./reports/*.json
	rm ./reports/*.xml

build_container:
	# --> Building container
	@docker build -t vss-policy-build -f ./Dockerfile.build . > /dev/null

refresh_certificate:
	# --> Refreshing CSP Token Signing Certificate
	@curl -L -s -o ./src/csp/cert.json https://console.cloud.vmware.com/csp/gateway/am/api/auth/token-public-key

bundle: refresh_certificate
	# --> Building Bundle Tarball
	@tar -czf ./bundles/opa-policies.tar.gz -C ./src/ .

test:
	# --> Running Tests
	@opa test --format=json ./src/ > ./reports/test.json

coverage:
	# --> Generating Coverage
	@opa test --format=json ./src/ --coverage > ./reports/coverage.json

check: test coverage

run: bundle check
	# --> Running OPA with generated bundle
	@opa run ./bundles/opa-policies.tar.gz

dev: build_container
	# --> Starting Dev Environment
	@docker run $(DOCKER_OPTS) /bin/bash
