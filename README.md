# vss-policy service

This isn't so much a service, as a configuration of existing services.

## Quickstart

#### Install OPA
Instructions are here: https://www.openpolicyagent.org/docs/latest/#running-opa
You can also use the dev container by running `make dev`

#### Run tests
```
# Create the bundle
make bundle

# Run tests
make test

# Run coverage
make coverage

# Run all tests
make check
```

## TODO List
TODO: The policy service should verify its own JWT's
TODO: The policy service should load the signing cert from CSP
TODO: We should enforce that the service id is valid
TODO: We should enforce that the signature is one we care about
TODO: We should enforce that the use has at least a minimum role
TODO: We should enforce that the token is not expired
TODO: We should enforce that JWT.iat is not in the future
TODO: We should enforce that JWT.nbf is not in the future.
TODO: We should enforce that the signature is of the expected type.
TODO: We should report the coverage to jenkins
TODO: We should report the tests to jenkins
TODO: We should publish the policies to S3
