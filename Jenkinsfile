/*
 * @copyright Copyright VMware, Inc. All rights reserved. VMware Confidential.
 */

@Library('vss-jenkins-lib') _

def slackThreadId

pipeline {

    agent {
        dockerfile {
            label 'ec2 && docker'
            filename 'Dockerfile.build'
        }
    }

    options {
        buildDiscarder(
                logRotator(numToKeepStr: '30', artifactNumToKeepStr: '30')
        )
        timeout(time: 1, unit: 'HOURS')
        timestamps()
    }

    environment {
        SLACK_CHANNEL = "${env.GIT_BRANCH == 'master' ? 'vss_releases' : 'vss_dev-activity'}"
        BITBUCKET_LINK = "${env.CHANGE_URL ?: env.GIT_URL}"

        // Required to checkout from bitbucket.
        GIT_SSH_COMMAND = "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

        JENKINS_CLIENT_CREDENTIALS = credentials('csp-jenkins-client-credentials')
    }

    stages {
        stage('Setup') {
            steps {
                parallel(
                        'Env': {
                            sh('env')
                            sh('opa version')
                            sh('pwd')
                        },
                        'Slack': {
                            script {
                                // First group, second value.
                                def slackResponse = slackSend(
                                        color: 'good',
                                        channel: SLACK_CHANNEL,
                                        message: "Build <${env.BUILD_URL}|${BUILD_DISPLAY_NAME}> for <${BITBUCKET_LINK}|${env.JOB_NAME}>."
                                )
                                slackThreadId = slackResponse.threadId
                            }
                        },
                        'Bundle': {
                            sh("make bundle")
                        }
                )
            }
        }

        stage('Test') {
            steps {
                parallel(
                        'Test: Unit': {
                            sh("make test")
                        },
                        'Test: Coverage': {
                            sh("make coverage")
                        }
                )
            }
        }
    }

    post {
        always {
            archiveArtifacts(
                    allowEmptyArchive: true,
                    artifacts: "reports/*",
                    excludes: null
            )
        }
        success {
            slackSend(color: 'good', message: 'Build Passed', channel: slackThreadId)
        }

        failure {
            slackSend(color: 'danger', message: 'Build Failed', channel: SLACK_CHANNEL)
        }

        unstable {
            slackSend(color: 'warning', message: 'Build Unstable', channel: slackThreadId)
        }

        aborted {
            slackSend(color: '#C2C5CC', message: 'Build Aborted', channel: slackThreadId)
        }

        cleanup {
            cleanWs()
        }
    }
}
